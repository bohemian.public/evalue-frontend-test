import { browser, by, element } from 'protractor';

export class AppRootPage {
  navigateTo () {
    return browser.get('/');
  }

  getCurrentUrl () {
    return browser.getCurrentUrl();
  }

  getParagraphText () {
    return element(by.css('app-root h1')).getText();
  }

  getNotes () {
    return element.all(by.css('app-root .card'));
  }

  clickNote () {
    element(by.css('app-root .card')).click();
    return browser.waitForAngular();
  }
}

export class AppAddNotePage {
  navigateTo () {
    return browser.get('/note/add');
  }

  getCurrentUrl () {
    return browser.getCurrentUrl();
  }

  getTextarea () {
    return element(by.css('app-root textarea'));
  }

  getSaveButton () {
    return element(by.css('app-root [data-test="add-note-button"]'));
  }

  fillAndSaveNote () {
    this.getTextarea().sendKeys('Hello world');
    this.getSaveButton().click();
    return browser.waitForAngular();
  }
}

export class AppViewNotePage {
  navigateTo () {
    return browser.get('/note/1');
  }

  getCurrentUrl () {
    return browser.getCurrentUrl();
  }

  getTextarea () {
    return element(by.css('app-root textarea'));
  }

  getSaveButton () {
    return element(by.css('app-root [data-test="save-note-button"]'));
  }

  getRemoveButton () {
    return element(by.css('app-root [data-test="remove-note-button"]'));
  }

  removeNote () {
    this.getRemoveButton().click();
    return browser.waitForAngular();
  }

  saveNote () {
    this.getSaveButton().click();
    return browser.waitForAngular();
  }
}
