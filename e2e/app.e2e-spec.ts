import { AppRootPage, AppAddNotePage, AppViewNotePage } from './app.po';

describe('e2e-test-root', () => {
  let page: AppRootPage;

  beforeEach(() => {
    page = new AppRootPage();
  });

  it('should display heading and two notes', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('My Notes');
    expect(page.getNotes().count()).toEqual(2);
  });

  it('should navigate to a note detail after clicking on it', () => {
    page.navigateTo();
    page.clickNote();
    expect(page.getCurrentUrl()).toContain('/note/1');
  });
});

describe('e2e-test-add', () => {
  let page: AppAddNotePage;

  beforeEach(() => {
    page = new AppAddNotePage();
  });

  it('should contain a textarea and disabled save button', () => {
    page.navigateTo();
    expect(page.getTextarea().getText()).toEqual('');
    expect(page.getSaveButton().getAttribute('disabled')).toEqual('true');
  });

  it('should save a note after filling something in', () => {
    page.navigateTo();
    page.fillAndSaveNote();
    expect(page.getCurrentUrl()).not.toContain('/notes/add');
  });
});

describe('e2e-test-view', () => {
  let page: AppViewNotePage;

  beforeEach(() => {
    page = new AppViewNotePage();
  });

  it('should contain a textarea filled with the note text', () => {
    page.navigateTo();
    expect(page.getTextarea().getAttribute('value')).toEqual('Pick-up posters from post-office');
  });

  it('should allow user to save the note', async function () {
    await page.navigateTo();
    expect(await page.getSaveButton().getText()).toEqual('Save');
    await page.saveNote();
    expect(await page.getCurrentUrl()).not.toContain('/note/1');
  });

  it('should allow user to remove the note', () => {
    page.navigateTo();
    page.removeNote();
    expect(page.getCurrentUrl()).not.toContain('/note/1');
  });
});
