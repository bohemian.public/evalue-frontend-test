### Intro

Připravte jednoduchou aplikaci v AngularJs (nebo ReactJS) tak, aby se dala po naklonování repositorya s nainstalovaným node.js nainstalovat a spustit.

### API

Předpřipravený server s REST API, které bude aplikace používat:

Root URL: http://private-9aad-note10.apiary-mock.com/

Metody:
GET /notes
GET /notes/{id}
POST /notes
PUT /notes/{id}
DELETE /notes/{id}

### Funkční požadavky:

[x] Po instalaci a spuštění se po zadání localhost:9000 objeví stránka se seznamem s poznámkami.

[x] Je možné zobrazit detail, editovat, smazat a vytvořit novou poznámku. (Apiary Mock bude vracet stále stejná data, předmětem úkolu je volat správné metody)

[x] V aplikaci bude možné měnit EN/CZ jazyk.

### Nefunkční požadavky:

[x] GUI dle vlastního návrhu, použití Bootstrapu a LESS/SASS/Stylus preferováno.

[x] Kód by měl být ES5+ JS s použitím novějších API jako Promise, Array extras. Ideální pokud by byl použit Typescript

[x] Instalace závislostí a build pomocí webpack nebo browserify.

[x] Templaty v js.

[x] Použít ui-router (nebo https://github.com/rackt/react-router).

[x] Alespoň jeden základní protractor test.

[x] Kód vyvíjejte do github/bitbucket veřejného repository, v souboru README.md popište instrukce proinstalaci a spuštění aplikace a testu, a pošlete URL emailem.
