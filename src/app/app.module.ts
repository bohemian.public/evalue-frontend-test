import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {UIRouterModule} from '@uirouter/angular';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

import { AppComponent } from './app.component';
import * as INITIAL_STATES from './states';
import { RootViewComponent } from './components/views/RootView.component';
import { NotesApiService } from './services/NotesApiService.service';
import { NoteViewComponent } from './components/views/NoteView.component';
import { AboutViewComponent } from './components/views/AboutView.component';
import { NoteThumbnailComponent } from './components/elements/NoteThumbnail.component';
import { AddNoteViewComponent } from './components/views/AddNoteView.component';

@NgModule({
  declarations: [
    AppComponent,
    RootViewComponent,
    NoteViewComponent,
    AboutViewComponent,
    AddNoteViewComponent,
    NoteThumbnailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    UIRouterModule.forRoot({
      states: [
        INITIAL_STATES.rootState,
        INITIAL_STATES.viewNoteState,
        INITIAL_STATES.aboutState,
        INITIAL_STATES.addNoteState
      ]
    })
  ],
  providers: [
    NotesApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
