import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  currentLanguage = 'en';
  translateSvc: TranslateService;

  constructor (translate: TranslateService) {
    let lang = 'en'
    translate.setDefaultLang(lang);
    if (localStorage && localStorage.getItem('lang')) {
      lang = localStorage.getItem('lang');
    }
    translate.use(lang);
    this.translateSvc = translate;
  }

  switchLanguage (lang: string) {
    this.currentLanguage = lang;
    this.translateSvc.use(lang);
    if (localStorage) {
      localStorage.setItem('lang', lang);
    }
  }

}
