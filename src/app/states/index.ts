import { RootViewComponent } from '../components/views/RootView.component';
import { NotesApiService } from '../services/NotesApiService.service';
import { NoteViewComponent } from '../components/views/NoteView.component';
import { Transition } from '@uirouter/core';
import { AboutViewComponent } from '../components/views/AboutView.component';
import { AddNoteViewComponent } from '../components/views/AddNoteView.component';

export const rootState = {
    name: 'root',
    url: '/',
    component: RootViewComponent,
    resolve: [
        {
            token: 'notes',
            deps: [NotesApiService],
            resolveFn: (notesApiSvc) => notesApiSvc.getNotes()
        }
    ]
};

export const viewNoteState = {
    name: 'viewNote',
    url: '/note/:id',
    component: NoteViewComponent,
    resolve: [
        {
            token: 'note',
            deps: [Transition, NotesApiService],
            resolveFn: (trans, notesApiSvc) => notesApiSvc.getNote(trans.params().id)
        }
    ]
};

export const addNoteState = {
    name: 'addNote',
    url: '/note/add',
    component: AddNoteViewComponent
};

export const aboutState = {
    name: 'about',
    url: '/about',
    component:  AboutViewComponent
};
