import { Component, Input } from '@angular/core';
import { Note } from '../../services/NotesApiService.service';

@Component({
  selector: 'app-note-thumbnail',
  template: `
    <a uiSref="viewNote" [uiParams]="{ id : note.id }" class="card">
        <div class="card-body">
            <p>{{note.title}}</p>
        </div>
    </a>
  `
})
export class NoteThumbnailComponent {
    @Input() note: Note;
}
