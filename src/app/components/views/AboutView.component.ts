import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-about-view',
  template: `
    <h1 translate>About</h1>
    <p translate>This is a simple notes application.</p>
  `
})
export class AboutViewComponent {
}
