import { Component, Input } from '@angular/core';
import { NotesApiService, Note } from '../../services/NotesApiService.service';
import { UIRouter, StateService } from '@uirouter/core';

@Component({
  selector: 'app-note-view',
  template: `
  <h1>{{ 'Note' | translate }} #{{ note.id }}</h1>
  <form (ngSubmit)="saveNote()">
    <div class="form-group">
      <textarea class="form-control" name="title" [(ngModel)]="note.title" required #title="ngModel"></textarea>

      <div [hidden]="title.valid || title.pristine" class="alert alert-danger" translate>
        Note has to be at least 1 character long!
      </div>
    </div>

    <div class="buttons-group float-left">
      <button
      (click)="deleteNote()"
      [disabled]="notesSvc.isWorking"
      class="btn btn-danger"
      data-test="remove-note-button" translate>
        Remove
      </button>
    </div>

    <div class="buttons-group float-right">
      <a uiSref="root" class="btn btn-secondary" translate>Back</a>
      <button
      type="submit"
      [disabled]="notesSvc.isWorking || title.invalid"
      class="btn btn-primary"
      data-test="save-note-button"
      translate>
        Save
      </button>
    </div>
  </form>

  `,
  styles: [
    `.form-group {margin: 2rem 0rem;}`
  ]
})
export class NoteViewComponent {
    @Input() note: Note;

    constructor (
      private notesSvc: NotesApiService,
      private $state: StateService
    ) {
    }

    async saveNote () {
      await this.notesSvc.saveNote(this.note);
      this.$state.go('root');
    }

    async deleteNote () {
      this.notesSvc.deleteNote(this.note);
      this.$state.go('root');
    }
}
