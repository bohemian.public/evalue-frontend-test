import { Component, Input } from '@angular/core';
import { NotesApiService, Note } from '../../services/NotesApiService.service';
import { StateService } from '@uirouter/core';

@Component({
  selector: 'app-note-view',
  template: `
  <h1 translate>Add Note</h1>
  <form (ngSubmit)="addNote()">
    <div class="form-group">
      <textarea class="form-control" name="title" [(ngModel)]="note.title" required #title="ngModel"></textarea>
      <div [hidden]="title.valid || title.pristine" class="alert alert-danger" translate>
        Note has to be at least 1 character long!
      </div>
    </div>

    <div class="buttons-group float-right">
      <a uiSref="root" class="btn btn-secondary" translate>Back</a>
      <button
      type="submit"
      [disabled]="notesSvc.isWorking || title.invalid"
      class="btn btn-primary"
      data-test="add-note-button" translate>
        Add
      </button>
    </div>
  </form>
  `,
  styles: [
    `.form-group {margin: 2rem 0rem;}`
  ]
})
export class AddNoteViewComponent {
    note: Note = {title: ''};

    constructor (
      private notesSvc: NotesApiService,
      private $state: StateService
    ) {}

    async addNote () {
      await this.notesSvc.addNote(this.note);
      this.$state.go('root');
    }
}
