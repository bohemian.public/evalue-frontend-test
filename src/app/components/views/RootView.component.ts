import { Component, Input } from '@angular/core';
import { Note } from '../../services/NotesApiService.service';

@Component({
  selector: 'app-root-view',
  template: `
  <div>
  <a uiSref="addNote" class="btn btn-success float-right" translate>Add Note</a>
  <h1 translate>My Notes</h1>
  </div>
  <app-note-thumbnail *ngFor="let note of notes" [note]="note">
  </app-note-thumbnail>
  `
})
export class RootViewComponent {
    @Input() notes: Note[];
}
