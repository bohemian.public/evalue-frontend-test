import {omit} from 'ramda';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

// const NOTES_API_URL = process.env.NOTES_API_URL || 'http://private-9aad-note10.apiary-mock.com'
const NOTES_API_URL = 'http://private-9aad-note10.apiary-mock.com';

export interface Note {
    id?: number;
    title: string;
}

@Injectable()
export class NotesApiService {
    constructor (private http: HttpClient) {
    }

    isWorking = false;

    getNotes () {
        this.isWorking = true;

        return this.http.get(`${NOTES_API_URL}/notes`)
            .toPromise()
            .then(r => {
                this.isWorking = false;
                return r;
            });
    }

    getNote (id: number) {
        this.isWorking = true;

        return this.http.get(`${NOTES_API_URL}/notes/${id}`)
            .toPromise()
            .then(r => {
                this.isWorking = false;
                return r;
            });
    }

    addNote (note: Note) {
        this.isWorking = true;

        return this.http.post(`${NOTES_API_URL}/notes`, omit(['id'], note))
            .toPromise()
            .then(r => {
                this.isWorking = false;
                return r;
            });
    }

    saveNote (note: Note) {
        this.isWorking = true;

        return this.http.put(`${NOTES_API_URL}/notes/${note.id}`, omit(['id'], note))
            .toPromise()
            .then(r => {
                this.isWorking = false;
                return r;
            });
    }

    deleteNote (note: Note) {
        this.isWorking = true;
        return this.http.delete(`${NOTES_API_URL}/notes/${note.id}`)
            .toPromise()
            .then(r => {
                this.isWorking = false;
                return r;
            });
    }
}
